import React, { useEffect, useRef, useState } from 'react'
import { Canvas, useFrame } from 'react-three-fiber'
import { OrbitControls, Stars } from 'drei'
import { Physics, useBox, usePlane } from 'use-cannon'
import { RingBufferGeometry } from 'three'


const Cylinder = ({ position, rotationSpeed, color, heightCylinder }) => {
  const mesh = useRef();
  const [ activeCylinder, setActiveCylinder ] = useState(false);
  useFrame(() => (mesh.current.rotation.x = mesh.current.rotation.y += rotationSpeed / 500));
  // const radiusTop = 4;
  // const radiusBottom = 4;
  // const height = 8;
  // const radialSegments = 12;
  // addSolidGeometry(1, 2, new THREE.CylinderBufferGeometry(radiusTop, radiusBottom, height, radialSegments));
  return (
    <mesh
      position={position}
      ref={mesh}
      scale={activeCylinder ? [ 1.5, 1.5, 1.5 ] : [ 1, 1, 1 ]}
      onClick={e => setActiveCylinder(!activeCylinder)}>
      <cylinderBufferGeometry attach="geometry" args={[1,1,heightCylinder,30]} />
      <meshPhongMaterial attach="material" color={color} />
    </mesh>
  )
}
const Ring = ({ position, rotationSpeed, color }) => {
  const mesh = useRef();
  useFrame(() => (mesh.current.rotation.z = mesh.current.rotation.z += rotationSpeed / 100));

  return (
    <mesh
      position={position}
      rotation={[ -Math.PI / 2, 0, 0 ]}
      ref={mesh}
      scale={[ 1, 1, 1 ]}>
      {/* <planeBufferGeometry attach="geometry" args={[100,100]} /> */}
      <ringBufferGeometry attach="geometry" args={[ 4, 4.35, 100 ]} />
      <meshPhongMaterial attach="material" color={color} />
    </mesh>
  )
}
const approximateColor1ToColor2ByPercent = (percent,color1='#FF0000', color2='#00FF00') => {
  percent /=100;
  if(percent < 0){
    color1='#00FF00';
    color2='#FF0000';
    percent = Math.abs(percent)
  }
  console.log("color1 after", color1)
  console.log("color2 after", color2)
  let red1 = parseInt(color1[1] + color1[2], 16);
  let green1 = parseInt(color1[3] + color1[4], 16);
  let blue1 = parseInt(color1[5] + color1[6], 16);

  let red2 = parseInt(color2[1] + color2[2], 16);
  let green2 = parseInt(color2[3] + color2[4], 16);
  let blue2 = parseInt(color2[5] + color2[6], 16);

  let red = Math.round(mix(red1, red2, percent));
  let green = Math.round(mix(green1, green2, percent));
  let blue = Math.round(mix(blue1, blue2, percent));
  return generateHex(red, green, blue);
}

const generateHex = (r, g, b) => {
  r = r.toString(16);
  g = g.toString(16);
  b = b.toString(16);

  // to address problem mentioned by Alexis Wilke:
  while (r.length < 2) { r = "0" + r; }
  while (g.length < 2) { g = "0" + g; }
  while (b.length < 2) { b = "0" + b; }

  return "#" + r + g + b;
}

const mix = (start, end, percent) => {
    return start + ((percent) * (end - start));
}
const cylindersList = [
  {
    position:[ -1, 2, 1 ],
    percentColor: 100,
    height:8
  },
  {
    position:[ 1, 2, -1 ],
    percentColor: -5,
    height:1
  },
  {
    position:[ -2, 0, -1 ],
    percentColor: 36,
    height:3
  },
  {
    position:[ -2, -12, -1.5 ],
    percentColor: -53,
    height:15
  },
  {
    position:[ 1.5, 0, -1.5 ],
    percentColor: -100,
    height:-6
  },
  {
    position:[ 2.2, 2, -2.2 ],
    percentColor: -44,
    height:-12
  }
]
export default function App() {
  return (
    <>
      <Canvas fov={1.5}>
        <OrbitControls />
        <ambientLight intensity={0.5} />
        <spotLight position={[ 10, 15, 10 ]} angle={0.3} />
        <Stars />
        <pointLight position={[ 10, 10, 10 ]} angle={0.15} penumbra={1} />
        <Physics>
          <Ring position={[ 0, 0, 0 ]} rotationSpeed={0} color="lightblue" />
          {cylindersList.map((item,inx)=>{
            return(
              <Cylinder key={inx} position={item.position} rotationSpeed={0} color={approximateColor1ToColor2ByPercent(item.percentColor)} heightCylinder={item.height} />
            )
          })}
         
        </Physics>

      </Canvas>
    </>
  )
}
